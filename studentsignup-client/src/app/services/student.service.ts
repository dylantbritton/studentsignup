import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Student } from '../models/student';
import { environment } from 'src/environments/environment';
import { throwError as observableThrowError} from 'rxjs';
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }


  register(student: Student) {
    const httpOptions = { 
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.http.post<Student>(this.apiUrl+ '/createStudent', student, httpOptions)
      .pipe(
        catchError((error: any ) => observableThrowError(error.json().error || 'Server error'))
      );
  }
}
