import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-register-student',
  templateUrl: './register-student.component.html',
  styleUrls: ['./register-student.component.css']
})
export class RegisterStudentComponent implements OnInit {

  registerForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;


  constructor(private studentService: StudentService) { }

  createFormControls() {
    this.firstName = new FormControl('', Validators.required);
    this.lastName =  new FormControl('', Validators.required);
  }

  createForm() {
    this.registerForm = new FormGroup({
      firstName: this.firstName,
      lastName: this.lastName,
    })
  }

  onSubmit(form) {
    this.studentService.register(form).subscribe(
      success => {
      console.log("Regster successful");
    }, err => {
      console.log(err);
    });
    this.registerForm.reset();
  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }
}
