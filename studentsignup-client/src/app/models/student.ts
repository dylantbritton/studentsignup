export class Student {

	firstName: String;
	lastName: String;
    
    constructor (firstName: String, lastName: String) {
            this.firstName = firstName;
            this.lastName = lastName;
    }

}
