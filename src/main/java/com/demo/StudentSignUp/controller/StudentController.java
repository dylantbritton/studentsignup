package com.demo.StudentSignUp.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.StudentSignUp.model.Student;

@CrossOrigin
@RestController
public class StudentController {
	
	@RequestMapping(value="/createStudent", method=RequestMethod.POST)
	public void student (@RequestBody Student student) {
		System.out.println("Received new student: " + student.getFirstName() + " " + student.getLastName());
	}

}
