package com.demo.StudentSignUp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.demo.StudentSignUp.controller,"
		+ "com.demo.StudentSignUp.model")
public class StudentSignUpApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentSignUpApplication.class, args);
	}

}
